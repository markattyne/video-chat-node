window.ElementUtil = {
	getPreview: function() {
		return document.getElementById('preview');
	},
	addRemoteVideo: function() {
		var videoElement = document.createElement('video');
		videoElement.autoplay = true;
		document.body.appendChild(videoElement);
		return videoElement;
	}
};