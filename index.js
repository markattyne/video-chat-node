var express = require('express');
var app = express();

app.use(express.static(__dirname + '/assets'));

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/views/index.html');
});

app.post('/room-participant/:roomId/:connectionId', function(req, res) {
	console.log(connections);
	var roomConnections = [];
	connections.forEach(function(connection) {
		if (connection.roomId == req.params.roomId) {
			roomConnections.push(connection.id);
		}
	});

	var connection = addConnection(req.params.connectionId);
	connection.roomId = req.params.roomId;

	res.send({ roomConnections: roomConnections });
});

var httpServer = require('http').Server(app);

httpServer.listen(3000, function() {
	console.log('listening on *:3000');
});



var ExpressPeerServer = require('peer').ExpressPeerServer;
var peerServer = ExpressPeerServer(httpServer, { debug: true, allow_discovery: true });
app.use('/peerjs', peerServer);

peerServer.on('connection', function(id) {
	console.log(id + ' connected!');
	addConnection(id);
	console.log(connections);
});

peerServer.on('disconnect', function(id) {
	console.log(id + ' disconnected!');
	removeConnection(id);
	console.log(connections);
});

var connections = [];

function addConnection(id) {
	for (var i = 0; i < connections.length; i++) {
		var connection = connections[i];
		if (connection.id == id) {
			return connection;
		}
	}
	var connection = { id: id };
	connections.push(connection);
	return connection;
}

function removeConnection(id) {
	for (var i = 0; i < connections.length; i++) {
		var connection = connections[i];
		if (connection.id == id) {
			connections.splice(i, 1);
			break;
		}
	}
}